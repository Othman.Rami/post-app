import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostListItemComponent } from './post-list-item/post-list-item.component';
import { Post } from './model/post';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import {Routes, RouterModule} from "@angular/router";
import { PostNewComponent } from './post-new/post-new.component';
import { PostsComponent } from './posts/posts.component';
import {PostService} from './services/posts-service'
const appRoutes:Routes=[
  {path:'posts',component:PostsComponent},
  {path:'new-post',component:PostNewComponent},
  {path:'',redirectTo:'posts',pathMatch:'full'}

]

@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    PostListItemComponent,
    HeaderComponent,
    PostNewComponent,
    PostsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
