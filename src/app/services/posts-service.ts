import { Post } from '../model/post';
import {Subject} from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import {DataSnapshot} from 'firebase/database';
@Injectable()
export class PostService{
  //  posts:Post[]= [new Post("Mon premier Post","content mon premier Post ",-1),new Post("Mon deuxieme Post","content deuxieme post",1) ];
  posts:Post[]=[];
  postSubject= new Subject<Post[]>();

  post:Post= new Post("","",0);
  emitPost(){
      this.postSubject.next(this.posts.slice());
  }

  addPost(post:Post){
      this.posts.push(post);
      this.savePosts();
      this.emitPost();
  }
  savePosts() {
    firebase.database().ref('/posts').set(this.posts);
}

getPosts() {
    firebase.database().ref('/posts')
      .on('value', (data: DataSnapshot) => {
          this.posts = data.val() ? data.val() : [];
          this.emitPost();
        }
      );
  }


  removePost(book: Post) {
    const bookIndexToRemove = this.posts.findIndex(
      (bookEl) => {
        if(bookEl === book) {
          return true;
        }
      }
    );
    this.posts.splice(bookIndexToRemove, 1);
    this.savePosts();
    this.emitPost();

  }
 
  updatePost(book: Post) {
    const bookIndexToRemove = this.posts.findIndex(
      (bookEl) => {
        if(bookEl === book) {
          bookEl.loveIts=book.loveIts;
          return true;
        }
      }
    );
    this.savePosts();
    this.emitPost();

  } 

} 