import { Component, OnInit } from '@angular/core';
import { Post } from '../model/post';
import { Subscription } from 'rxjs/Subscription';
import { PostService } from '../services/posts-service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts:Post[];
  postSubscription:Subscription;
  constructor(private postService:PostService) { }

  ngOnInit() {
    this.postSubscription= this.postService.postSubject.subscribe(
      (response: Post[])=>{
        console.log("receiving data");
        this.posts= response;
      },
      (error)=>{
        console.log("Erreur ! "+error);
      }
    )

this.postService.getPosts();
  }
  ngOnDestroy() {
    this.postSubscription.unsubscribe();
  }
}
