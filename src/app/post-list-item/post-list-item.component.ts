import { Component, OnInit,Input } from '@angular/core';
import { Post } from '../model/post';
import { PostService } from '../services/posts-service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() postitem:Post;
  constructor(private postService:PostService) { }

  ngOnInit() {
  }
  onLike(){
    this.postitem.loveIts= this.postitem.loveIts+1;
    this.postService.updatePost(this.postitem);
  }
  onDislike(){
    this.postitem.loveIts=this.postitem.loveIts-1;
    this.postService.updatePost(this.postitem);
  }
  onDeletePost(post:Post){
    this.postService.removePost(post);
  }

}
