import { Component } from '@angular/core';
import * as firebase from 'firebase';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  constructor(){
    var config = {
      apiKey: "AIzaSyAJo442O9u449xBOM-FUfq3QFW4Be8o1YE",
      authDomain: "books-livre.firebaseapp.com",
      databaseURL: "https://books-livre.firebaseio.com",
      projectId: "books-livre",
      storageBucket: "",
      messagingSenderId: "453715023295"
    };
    firebase.initializeApp(config);
  
  }
}
