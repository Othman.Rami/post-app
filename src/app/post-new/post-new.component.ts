import { Component, OnInit } from '@angular/core';
import { FormGroup , FormBuilder, Validators } from '@angular/forms';
import {Router} from '@angular/router';
import { PostService } from '../services/posts-service';
import { Post } from '../model/post';
@Component({
  selector: 'app-post-new',
  templateUrl: './post-new.component.html',
  styleUrls: ['./post-new.component.scss']
})
export class PostNewComponent implements OnInit {
addpostForm : FormGroup;

  constructor(private router:Router,private formBuilder:FormBuilder,private postService:PostService) { }

  ngOnInit() {
    this.initForm(); }

    initForm(){
      this.addpostForm= this.formBuilder.group({
        title:['',[Validators.required]],
        content:['',[Validators.required]]
      })
    }

  onSubmit(){
    const title = this.addpostForm.get('title').value;
    const content = this.addpostForm.get('content').value;
    let post = new Post(title,content,0);
    console.log(title+" "+content);
    this.postService.addPost(post);
    this.router.navigate(['/posts']);
  }  
}
